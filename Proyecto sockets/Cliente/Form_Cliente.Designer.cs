﻿namespace Cliente
{
    partial class Form_Cliente
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.BtnDesconectar = new System.Windows.Forms.Button();
            this.LblRecibir = new System.Windows.Forms.Label();
            this.TxtRecibirMs = new System.Windows.Forms.TextBox();
            this.BtnEnviarMs = new System.Windows.Forms.Button();
            this.LblStatus = new System.Windows.Forms.Label();
            this.BtnConectar = new System.Windows.Forms.Button();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // BtnDesconectar
            // 
            this.BtnDesconectar.BackColor = System.Drawing.Color.Violet;
            this.BtnDesconectar.Location = new System.Drawing.Point(64, 0);
            this.BtnDesconectar.Name = "BtnDesconectar";
            this.BtnDesconectar.Size = new System.Drawing.Size(119, 26);
            this.BtnDesconectar.TabIndex = 31;
            this.BtnDesconectar.Text = "Salir del Servidor";
            this.BtnDesconectar.UseVisualStyleBackColor = false;
            this.BtnDesconectar.Click += new System.EventHandler(this.BtnDesconectar_Click);
            // 
            // LblRecibir
            // 
            this.LblRecibir.AutoSize = true;
            this.LblRecibir.ForeColor = System.Drawing.Color.White;
            this.LblRecibir.Location = new System.Drawing.Point(137, 157);
            this.LblRecibir.Name = "LblRecibir";
            this.LblRecibir.Size = new System.Drawing.Size(142, 13);
            this.LblRecibir.TabIndex = 30;
            this.LblRecibir.Text = "Mensaje a enviar al sevvidor";
            // 
            // TxtRecibirMs
            // 
            this.TxtRecibirMs.Location = new System.Drawing.Point(45, 173);
            this.TxtRecibirMs.Multiline = true;
            this.TxtRecibirMs.Name = "TxtRecibirMs";
            this.TxtRecibirMs.Size = new System.Drawing.Size(342, 37);
            this.TxtRecibirMs.TabIndex = 29;
            // 
            // BtnEnviarMs
            // 
            this.BtnEnviarMs.BackColor = System.Drawing.Color.Violet;
            this.BtnEnviarMs.Location = new System.Drawing.Point(149, 106);
            this.BtnEnviarMs.Name = "BtnEnviarMs";
            this.BtnEnviarMs.Size = new System.Drawing.Size(119, 26);
            this.BtnEnviarMs.TabIndex = 28;
            this.BtnEnviarMs.Text = "Resolver Operacion";
            this.BtnEnviarMs.UseVisualStyleBackColor = false;
            this.BtnEnviarMs.Click += new System.EventHandler(this.BtnEnviarMs_Click);
            // 
            // LblStatus
            // 
            this.LblStatus.AutoSize = true;
            this.LblStatus.Location = new System.Drawing.Point(1, 213);
            this.LblStatus.Name = "LblStatus";
            this.LblStatus.Size = new System.Drawing.Size(75, 13);
            this.LblStatus.TabIndex = 25;
            this.LblStatus.Text = "No conectado";
            // 
            // BtnConectar
            // 
            this.BtnConectar.BackColor = System.Drawing.Color.Violet;
            this.BtnConectar.Location = new System.Drawing.Point(241, 0);
            this.BtnConectar.Name = "BtnConectar";
            this.BtnConectar.Size = new System.Drawing.Size(119, 26);
            this.BtnConectar.TabIndex = 24;
            this.BtnConectar.Text = "Conectar al Servidor";
            this.BtnConectar.UseVisualStyleBackColor = false;
            this.BtnConectar.Click += new System.EventHandler(this.BtnConectar_Click);
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(45, 69);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(148, 20);
            this.textBox1.TabIndex = 32;
            // 
            // textBox2
            // 
            this.textBox2.Location = new System.Drawing.Point(222, 69);
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(148, 20);
            this.textBox2.TabIndex = 33;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.ForeColor = System.Drawing.Color.White;
            this.label1.Location = new System.Drawing.Point(60, 53);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(110, 13);
            this.label1.TabIndex = 34;
            this.label1.Text = "Ingresa el primer valor";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.ForeColor = System.Drawing.Color.White;
            this.label2.Location = new System.Drawing.Point(229, 53);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(123, 13);
            this.label2.TabIndex = 35;
            this.label2.Text = "Ingresa el segundo valor";
            // 
            // Form_Cliente
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Purple;
            this.ClientSize = new System.Drawing.Size(430, 234);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.textBox2);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.BtnDesconectar);
            this.Controls.Add(this.LblRecibir);
            this.Controls.Add(this.TxtRecibirMs);
            this.Controls.Add(this.BtnEnviarMs);
            this.Controls.Add(this.LblStatus);
            this.Controls.Add(this.BtnConectar);
            this.Name = "Form_Cliente";
            this.Text = "Cliente";
            this.Load += new System.EventHandler(this.Form_Cliente_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button BtnDesconectar;
        private System.Windows.Forms.Label LblRecibir;
        private System.Windows.Forms.TextBox TxtRecibirMs;
        private System.Windows.Forms.Button BtnEnviarMs;
        private System.Windows.Forms.Label LblStatus;
        private System.Windows.Forms.Button BtnConectar;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
    }
}

