﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Cliente
{
    public partial class Form_Cliente : Form
    {
        // Receiving byte array  
        byte[] bytes = new byte[1024];
        Socket senderSock;

        public Form_Cliente()
        {
            InitializeComponent();
            BtnEnviarMs.Enabled = false;
            BtnDesconectar.Enabled = false;
        }

        private void BtnConectar_Click(object sender, EventArgs e)
        {
            try
            {
                // Create one SocketPermission for socket access restrictions 
                SocketPermission permission = new SocketPermission(
                    NetworkAccess.Connect,    // Connection permission 
                    TransportType.Tcp,        // Defines transport types 
                    "",                       // Gets the IP addresses 
                    SocketPermission.AllPorts // All ports 
                    );

                // Ensures the code to have permission to access a Socket 
                permission.Demand();

                // Resolves a host name to an IPHostEntry instance            
                IPHostEntry ipHost = Dns.GetHostEntry("");

                // Gets first IP address associated with a localhost 
                IPAddress ipAddr = ipHost.AddressList[0];

                // Creates a network endpoint 
                IPEndPoint ipEndPoint = new IPEndPoint(ipAddr, 4510);

                // Create one Socket object to setup Tcp connection 
                senderSock = new Socket(
                    ipAddr.AddressFamily,// Specifies the addressing scheme 
                    SocketType.Stream,   // The type of socket  
                    ProtocolType.Tcp     // Specifies the protocols  
                    );

                senderSock.NoDelay = false;   // Using the Nagle algorithm 

                // Establishes a connection to a remote host 
                senderSock.Connect(ipEndPoint);
                LblStatus.Text = "Socket conectado a " + senderSock.RemoteEndPoint.ToString();

                BtnConectar.Enabled = false;
                BtnEnviarMs.Enabled = true;
            }
            catch (Exception exc) { MessageBox.Show(exc.ToString()); }
        }

        private void BtnEnviarMs_Click(object sender, EventArgs e)
        {
            try
            {
               
                // Sending message 
                //<Client Quit> is the sign for end of data
                string theMessageToSend =textBox1.Text+" , " +textBox2.Text;
                byte[] msg = Encoding.Unicode.GetBytes(theMessageToSend);


                // Sends data to a connected Socket. 
                int bytesSend = senderSock.Send(msg); 
                //int bytesSend2 = senderSock.Send(msg2);

                ReceiveDataFromServer();

                BtnEnviarMs.Enabled = false;
                BtnDesconectar.Enabled = true;
            }
            catch (Exception exc) { MessageBox.Show(exc.ToString()); }
        }

        private void ReceiveDataFromServer()
        {
            try
            {
                // Receives data from a bound Socket. 
                int bytesRec = senderSock.Receive(bytes);

                // Converts byte array to string 
                String theMessageToReceive = Encoding.Unicode.GetString(bytes, 0, bytesRec);

                // Continues to read the data till data isn't available 
                while (senderSock.Available > 0)
                {
                    bytesRec = senderSock.Receive(bytes);
                    theMessageToReceive += Encoding.Unicode.GetString(bytes, 0, bytesRec);
                }
                TxtRecibirMs.Text = "El Resultado de la multiplicacion es: " + theMessageToReceive;
                //textBox2.Text= "La respuesta del servidor: " + theMessageToReceive;
            }
            catch (Exception exc) { MessageBox.Show(exc.ToString()); }
        }

        private void BtnDesconectar_Click(object sender, EventArgs e)
        {
            try
            {
                // Disables sends and receives on a Socket. 
                senderSock.Shutdown(SocketShutdown.Both);

                //Closes the Socket connection and releases all resources 
                senderSock.Close();

                BtnDesconectar.Enabled = false;
                Close();
            }
            catch (Exception exc) { MessageBox.Show(exc.ToString()); }
        }

        private void Form_Cliente_Load(object sender, EventArgs e)
        {

        }
    }
}
