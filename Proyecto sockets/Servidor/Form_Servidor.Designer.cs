﻿namespace Servidor
{
    partial class Form_Servidor
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.BtnEmpezarEscuchar = new System.Windows.Forms.Button();
            this.BtnDesconectar = new System.Windows.Forms.Button();
            this.LblRecibir = new System.Windows.Forms.Label();
            this.TxtEnviarMs = new System.Windows.Forms.TextBox();
            this.BtnEnviarMs = new System.Windows.Forms.Button();
            this.LblStatus = new System.Windows.Forms.Label();
            this.BtnEmpezar = new System.Windows.Forms.Button();
            this.txt2 = new System.Windows.Forms.TextBox();
            this.txt3 = new System.Windows.Forms.TextBox();
            this.txt4 = new System.Windows.Forms.TextBox();
            this.txtresultado = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // BtnEmpezarEscuchar
            // 
            this.BtnEmpezarEscuchar.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.BtnEmpezarEscuchar.Location = new System.Drawing.Point(12, 57);
            this.BtnEmpezarEscuchar.Name = "BtnEmpezarEscuchar";
            this.BtnEmpezarEscuchar.Size = new System.Drawing.Size(119, 26);
            this.BtnEmpezarEscuchar.TabIndex = 34;
            this.BtnEmpezarEscuchar.Text = "Empieza a escuchar";
            this.BtnEmpezarEscuchar.UseVisualStyleBackColor = false;
            this.BtnEmpezarEscuchar.Click += new System.EventHandler(this.BtnEmpezarEscuchar_Click);
            // 
            // BtnDesconectar
            // 
            this.BtnDesconectar.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.BtnDesconectar.ForeColor = System.Drawing.SystemColors.ControlDark;
            this.BtnDesconectar.Location = new System.Drawing.Point(100, 247);
            this.BtnDesconectar.Name = "BtnDesconectar";
            this.BtnDesconectar.Size = new System.Drawing.Size(102, 26);
            this.BtnDesconectar.TabIndex = 33;
            this.BtnDesconectar.Text = "Salir";
            this.BtnDesconectar.UseVisualStyleBackColor = false;
            this.BtnDesconectar.Click += new System.EventHandler(this.BtnDesconectar_Click);
            // 
            // LblRecibir
            // 
            this.LblRecibir.AutoSize = true;
            this.LblRecibir.Location = new System.Drawing.Point(85, 112);
            this.LblRecibir.Name = "LblRecibir";
            this.LblRecibir.Size = new System.Drawing.Size(166, 13);
            this.LblRecibir.TabIndex = 32;
            this.LblRecibir.Text = "Enviar Resultado de la Operacion";
            // 
            // TxtEnviarMs
            // 
            this.TxtEnviarMs.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.TxtEnviarMs.Location = new System.Drawing.Point(39, 166);
            this.TxtEnviarMs.Multiline = true;
            this.TxtEnviarMs.Name = "TxtEnviarMs";
            this.TxtEnviarMs.Size = new System.Drawing.Size(222, 75);
            this.TxtEnviarMs.TabIndex = 31;
            // 
            // BtnEnviarMs
            // 
            this.BtnEnviarMs.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.BtnEnviarMs.ForeColor = System.Drawing.Color.Black;
            this.BtnEnviarMs.Location = new System.Drawing.Point(88, 141);
            this.BtnEnviarMs.Name = "BtnEnviarMs";
            this.BtnEnviarMs.Size = new System.Drawing.Size(114, 26);
            this.BtnEnviarMs.TabIndex = 30;
            this.BtnEnviarMs.Text = " Enviar";
            this.BtnEnviarMs.UseVisualStyleBackColor = false;
            this.BtnEnviarMs.Click += new System.EventHandler(this.BtnEnviarMs_Click);
            // 
            // LblStatus
            // 
            this.LblStatus.AutoSize = true;
            this.LblStatus.Location = new System.Drawing.Point(0, 0);
            this.LblStatus.Name = "LblStatus";
            this.LblStatus.Size = new System.Drawing.Size(75, 13);
            this.LblStatus.TabIndex = 27;
            this.LblStatus.Text = "No conectado";
            // 
            // BtnEmpezar
            // 
            this.BtnEmpezar.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.BtnEmpezar.Location = new System.Drawing.Point(12, 25);
            this.BtnEmpezar.Name = "BtnEmpezar";
            this.BtnEmpezar.Size = new System.Drawing.Size(119, 26);
            this.BtnEmpezar.TabIndex = 26;
            this.BtnEmpezar.Text = "Empieza Servidor";
            this.BtnEmpezar.UseVisualStyleBackColor = false;
            this.BtnEmpezar.Click += new System.EventHandler(this.BtnEmpezar_Click);
            // 
            // txt2
            // 
            this.txt2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.txt2.Location = new System.Drawing.Point(147, 47);
            this.txt2.Name = "txt2";
            this.txt2.Size = new System.Drawing.Size(138, 20);
            this.txt2.TabIndex = 37;
            // 
            // txt3
            // 
            this.txt3.Location = new System.Drawing.Point(28, 102);
            this.txt3.Name = "txt3";
            this.txt3.Size = new System.Drawing.Size(103, 20);
            this.txt3.TabIndex = 42;
            this.txt3.Visible = false;
            // 
            // txt4
            // 
            this.txt4.Location = new System.Drawing.Point(158, 102);
            this.txt4.Name = "txt4";
            this.txt4.Size = new System.Drawing.Size(103, 20);
            this.txt4.TabIndex = 43;
            this.txt4.Visible = false;
            this.txt4.TextChanged += new System.EventHandler(this.txt4_TextChanged);
            // 
            // txtresultado
            // 
            this.txtresultado.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.txtresultado.Location = new System.Drawing.Point(53, 115);
            this.txtresultado.Name = "txtresultado";
            this.txtresultado.Size = new System.Drawing.Size(189, 20);
            this.txtresultado.TabIndex = 44;
            this.txtresultado.Visible = false;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(85, 99);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(131, 13);
            this.label1.TabIndex = 45;
            this.label1.Text = "Resultado de la operacion";
            this.label1.Visible = false;
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.button1.ForeColor = System.Drawing.Color.White;
            this.button1.Location = new System.Drawing.Point(169, 73);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(92, 23);
            this.button1.TabIndex = 41;
            this.button1.Text = "Resolver Operacion";
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Visible = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.ForeColor = System.Drawing.Color.White;
            this.label2.Location = new System.Drawing.Point(166, 25);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(106, 13);
            this.label2.TabIndex = 46;
            this.label2.Text = "Operacion a Realizar";
            // 
            // Form_Servidor
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            this.ClientSize = new System.Drawing.Size(297, 308);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txtresultado);
            this.Controls.Add(this.txt4);
            this.Controls.Add(this.txt3);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.txt2);
            this.Controls.Add(this.BtnEmpezarEscuchar);
            this.Controls.Add(this.BtnDesconectar);
            this.Controls.Add(this.LblRecibir);
            this.Controls.Add(this.TxtEnviarMs);
            this.Controls.Add(this.BtnEnviarMs);
            this.Controls.Add(this.LblStatus);
            this.Controls.Add(this.BtnEmpezar);
            this.Name = "Form_Servidor";
            this.Text = "Servidor";
            this.Load += new System.EventHandler(this.Form_Servidor_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button BtnEmpezarEscuchar;
        private System.Windows.Forms.Button BtnDesconectar;
        private System.Windows.Forms.Label LblRecibir;
        private System.Windows.Forms.TextBox TxtEnviarMs;
        private System.Windows.Forms.Button BtnEnviarMs;
        private System.Windows.Forms.Label LblStatus;
        private System.Windows.Forms.Button BtnEmpezar;
        private System.Windows.Forms.TextBox txt2;
        private System.Windows.Forms.TextBox txt3;
        private System.Windows.Forms.TextBox txt4;
        private System.Windows.Forms.TextBox txtresultado;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label label2;
    }
}

