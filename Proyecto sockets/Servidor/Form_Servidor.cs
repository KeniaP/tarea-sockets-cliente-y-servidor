﻿using SimpleTCP;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Text.RegularExpressions;

namespace Servidor
{
    public partial class Form_Servidor : Form
    {
        SocketPermission permission;
        Socket sListener;
        IPEndPoint ipEndPoint;
        Socket handler;

        //private TextBox tbAux = new TextBox();

        SimpleTcpServer Servidor;


        public Form_Servidor()
        {
            InitializeComponent();
            //tbAux.SelectionChanged += tbAux_SelectionChanged;

            BtnEmpezar.Enabled = true;
            BtnEmpezarEscuchar.Enabled = false;
            BtnEnviarMs.Enabled = false;
            BtnDesconectar.Enabled = false;

        }
        private void Form_Servidor_Load(object sender, EventArgs e)
        {
            Servidor = new SimpleTcpServer();
            Servidor.Delimiter = 0x13;//enter
            Servidor.DataReceived += Server_DataRecivided;
        }

        private void Server_DataRecivided(object sender, SimpleTCP.Message e)
        {
            txt2.Invoke((MethodInvoker)delegate ()
            {
                txt2.Text += e.MessageString;
                e.ReplyLine(string.Format("Tu dice: {0}", e.MessageString));
                txt2.Text += e.MessageString;
                e.ReplyLine(string.Format("Tu dice: {0}", e.MessageString));
            });
            //txt2.Invoke((MethodInvoker)delegate ()
            //{
            //    txt2.Text += e.MessageString;
            //    e.ReplyLine(string.Format("Tu dice: {0}", e.MessageString));
            //}
            //);
        }

        private void BtnEmpezar_Click(object sender, EventArgs e)
        {
            try
            {
                // Creates one SocketPermission object for access restrictions
                permission = new SocketPermission(
                NetworkAccess.Accept,     // Allowed to accept connections 
                TransportType.Tcp,        // Defines transport types 
                "",                       // The IP addresses of local host 
                SocketPermission.AllPorts // Specifies all ports 
                );

                // Listening Socket object 
                sListener = null;

                // Ensures the code to have permission to access a Socket 
                permission.Demand();

                // Resolves a host name to an IPHostEntry instance 
                IPHostEntry ipHost = Dns.GetHostEntry("");

                // Gets first IP address associated with a localhost 
                IPAddress ipAddr = ipHost.AddressList[0];

                // Creates a network endpoint 
                ipEndPoint = new IPEndPoint(ipAddr, 4510);

                // Create one Socket object to listen the incoming connection 
                sListener = new Socket(
                    ipAddr.AddressFamily,
                    SocketType.Stream,
                    ProtocolType.Tcp
                    );

                // Associates a Socket with a local endpoint 
                sListener.Bind(ipEndPoint);

                LblStatus.Text = "Server started.";

                BtnEmpezar.Enabled = false;
                BtnEmpezarEscuchar.Enabled = true;
            }
            catch (Exception exc) { MessageBox.Show(exc.ToString()); }
        }

        private void BtnEmpezarEscuchar_Click(object sender, EventArgs e)
        {
            try
            {
                // Places a Socket in a listening state and specifies the maximum 
                // Length of the pending connections queue 
                sListener.Listen(10);

                // Begins an asynchronous operation to accept an attempt 
                AsyncCallback aCallback = new AsyncCallback(AcceptCallback);
                sListener.BeginAccept(aCallback, sListener);

                LblStatus.Text = "Server is now listening on " + ipEndPoint.Address + " port: " + ipEndPoint.Port;

                BtnEmpezarEscuchar.Enabled = false;
                BtnEnviarMs.Enabled = true;
            }
            catch (Exception exc) { MessageBox.Show(exc.ToString()); }
        }

        public void AcceptCallback(IAsyncResult ar)
        {
            Socket listener = null;

            // A new Socket to handle remote host communication 
            Socket handler = null;
            try
            {
                // Receiving byte array 
                byte[] buffer = new byte[1024];
                // Get Listening Socket object 
                listener = (Socket)ar.AsyncState;
                // Create a new socket 
                handler = listener.EndAccept(ar);

                // Using the Nagle algorithm 
                handler.NoDelay = false;

                // Creates one object array for passing data 
                object[] obj = new object[2];
                obj[0] = buffer;
                obj[1] = handler;

                // Begins to asynchronously receive data 
                handler.BeginReceive(
                    buffer,        // An array of type Byt for received data 
                    0,             // The zero-based position in the buffer  
                    buffer.Length, // The number of bytes to receive 
                    SocketFlags.None,// Specifies send and receive behaviors 
                    new AsyncCallback(ReceiveCallback),//An AsyncCallback delegate 
                    obj            // Specifies infomation for receive operation 
                    );

                // Begins an asynchronous operation to accept an attempt 
                AsyncCallback aCallback = new AsyncCallback(AcceptCallback);
                listener.BeginAccept(aCallback, listener);
            }
            catch (Exception exc) { MessageBox.Show(exc.ToString()); }
        }

        public void ReceiveCallback(IAsyncResult ar)
        {
            try
            {
                // Fetch a user-defined object that contains information 
                object[] obj = new object[2];
                obj = (object[])ar.AsyncState;

                // Received byte array 
                byte[] buffer = (byte[])obj[0];

                // A Socket to handle remote host communication. 
                handler = (Socket)obj[1];

                // Received message 
                string content = string.Empty;


                // The number of bytes received. 
                int bytesRead = handler.EndReceive(ar);

                if (bytesRead > 0)
                {
                    content += Encoding.Unicode.GetString(buffer, 0,
                        bytesRead);

                    // If message contains "<Client Quit>", finish receiving
                    if (content.IndexOf("<Client Quit>") > -1)
                    {
                        // Convert byte array to string
                        string str = content.Substring(0, content.LastIndexOf("<Client Quit>"));

                        //this is used because the UI couldn't be accessed from an external Thread
                        txt2.Invoke((MethodInvoker)delegate ()
                         {
                             string texbox = Convert.ToString(txt2.Text + txt2.Text);
                             texbox = "Read " + str.Length * 2 + " bytes from client.\n Data: " + str;
                         }
                         );
                        //txt2.Invoke((MethodInvoker)delegate ()
                        //    {
                        //        txt2.Text = "Read " + str.Length * 2 + " bytes from client.\n Data: " + str;
                        //    }

                        //    );
                    }
                    else
                    {
                        // Continues to asynchronously receive data
                        byte[] buffernew = new byte[1024];
                        obj[0] = buffernew;
                        obj[1] = handler;
                        handler.BeginReceive(buffernew, 0, buffernew.Length,
                            SocketFlags.None,
                            new AsyncCallback(ReceiveCallback), obj);
                    }

                    txt2.Invoke((MethodInvoker)delegate ()
                    {
                        txt2.Text = content;
                    }

                    );
                    //txt2.Invoke((MethodInvoker)delegate ()
                    //{
                    //    txt2.Text = content;
                    //}

                    //);
                }
            }
            catch (Exception exc) { MessageBox.Show(exc.ToString()); }
        }

        private void BtnEnviarMs_Click(object sender, EventArgs e)
        {
            try
            {
                string s = txt2.Text;// extraer informacion
                string[] numbers = s.Split(',');// como identificas donde vas a separar 
                string primerdigito = numbers[0];
                string segundodigito = numbers[1];
                txt3.Text = primerdigito;
                txt4.Text = segundodigito;
                decimal texbox1 = Convert.ToDecimal(txt3.Text);
                decimal texbox2 = Convert.ToDecimal(txt4.Text);
                decimal multiplicacion;
                multiplicacion = texbox1 * texbox2;
                txtresultado.Text = multiplicacion.ToString();
                //decimal texbox1 = Convert.ToDecimal(txt3.Text);
                //decimal texbox2 = Convert.ToDecimal(txt4.Text);
                //decimal multiplicacion;
                //multiplicacion = texbox1 * texbox2;
                TxtEnviarMs.Text = multiplicacion.ToString();

                // Convert byte array to string 
                string str = TxtEnviarMs.Text;

                // Prepare the reply message 
                byte[] byteData =
                    Encoding.Unicode.GetBytes(str);

                // Sends data asynchronously to a connected Socket 
                handler.BeginSend(byteData, 0, byteData.Length, 0,
                    new AsyncCallback(SendCallback), handler);

                BtnEnviarMs.Enabled = false;
                BtnDesconectar.Enabled = true;
            }
            catch (Exception exc) { MessageBox.Show(exc.ToString()); }
        }

        public void SendCallback(IAsyncResult ar)
        {
            try
            {
                // A Socket which has sent the data to remote host 
                Socket handler = (Socket)ar.AsyncState;

                // The number of bytes sent to the Socket 
                int bytesSend = handler.EndSend(ar);
                Console.WriteLine(
                    "Sent {0} bytes to Client", bytesSend);
            }
            catch (Exception exc) { MessageBox.Show(exc.ToString()); }
        }

        private void BtnDesconectar_Click(object sender, EventArgs e)
        {
            try
            {
                if (sListener.Connected)
                {
                    sListener.Shutdown(SocketShutdown.Receive);
                    sListener.Close();
                }

                BtnDesconectar.Enabled = false;
                Close();
            }
            catch (Exception exc) { MessageBox.Show(exc.ToString()); }
        }
        public static bool IsNumeric(string sValue)
        {
            return Regex.IsMatch(sValue, "^[0-9]+$");
        }
        private void button1_Click(object sender, EventArgs e)
        {
            //decimal n3 = Convert.ToDecimal(txt3.Text);
            //decimal result = 0;
            //decimal.TryParse(Regex.Match(n3, @"\d+").Value, out result);
            string s = txt2.Text;// extraer informacion
            string[] numbers = s.Split(',');// como identificas donde vas a separar 
            string primerdigito = numbers[0];
            string segundodigito = numbers[1];
            txt3.Text = primerdigito;
            txt4.Text = segundodigito;
            decimal texbox1 = Convert.ToDecimal(txt3.Text);
            decimal texbox2 = Convert.ToDecimal(txt4.Text);
            decimal multiplicacion;
           multiplicacion = texbox1 * texbox2;
            txtresultado.Text = multiplicacion.ToString();
        }

        private void txt4_TextChanged(object sender, EventArgs e)
        {

        }
    }
    }


